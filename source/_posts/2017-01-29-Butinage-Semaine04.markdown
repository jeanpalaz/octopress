---
layout: post
title: "Mes butinages de la semaine 04"
date: 2017-01-29 00:42:00 +0000
comments: true
categories: [Blog, Test, Junit, legacy, code, softwarecraftmanship, technical, debt]
---

Voilà, je fais part de mes butinages hebdomadaires. Dans mon cheminement d'agiliste, j'ai décidé de ne plus envoyé mes lectures à mes connaissances.
Je passe d'un flux poussé à un flux tiré :-)
Ce qui veut dire que je vais publier cette collecte et viendront voir ceux qui sont intéréssés.

**Bonne butinage !**

# Butinages de la semaine 04!

## Artisanat logiciel
* [Clean Code – Does it really matter ?](http://www.thewayofthecraftsman.com/clean-code-does-it-really-matter/)
* [Dans les entrailles de Git](http://nicolas.loeuillet.org/billets/entrailles-git)
* [I love bug reports](https://dascritch.net/post/2013/12/10/I-love-bug-reports)
* [Introduction aux structures algébriques](https://derniercri.io/tech-blog/structure-algebriques-introduction)

## Vie privée
* [Si on laissait tomber Facebook ?](https://framablog.org/2017/01/23/si-on-laissait-tomber-facebook/https://framablog.org/2017/01/23/si-on-laissait-tomber-facebook/)
* [Chiffrement, sécurité et libertés, positionnement de l’Observatoire des libertés et du Numérique](https://www.laquadrature.net/fr/oln-positionnement-chiffrement)

## Divers
* [Eloigner les jeunes enfants des tablettes, "un enjeu de santé publique"](http://rmc.bfmtv.com/emission/eloigner-les-jeunes-enfants-des-tablettes-un-enjeu-de-sante-publique-915265.html)
* [Des jeunes diplômés en quête de sens bousculent le monde du travail](http://www.lemonde.fr/o21/article/2017/01/17/des-jeunes-diplomes-en-quete-de-sens-bousculent-le-monde-du-travail_5064057_5014018.html)
