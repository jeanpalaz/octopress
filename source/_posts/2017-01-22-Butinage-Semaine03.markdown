---
layout: post
title: "Mes butinages de la semaine 03"
date: 2017-01-22 00:42:00 +0000
comments: true
categories: [Blog, Test, Junit, legacy, code, softwarecraftmanship, technical, debt]
---

Voilà, je fais part de mes butinages hebdomadaires. Dans mon cheminement d'agiliste, j'ai décidé de ne plus envoyé mes lectures à mes connaissances.
Je passe d'un flux poussé à un flux tiré :-)
Ce qui veut dire que je vais publier cette collecte et viendront voir ceux qui sont intéréssés.

**Bonne butinage !**

# Butinages de la semaine 03!

## Artisanat logiciel
* [Les entrailles de Git](http://nicolas.loeuillet.org/billets/entrailles-git)
* [Open-source developers deserve respect](https://codurance.com/2011/05/05/open-source-developers-deserve-respect/)
* [BDD](https://www.youtube.com/watch?v=_sNZLTMO8n0)
* [Git rebase, le couteau suisse de votre historique](https://www.miximum.fr/blog/git-rebase/)

## Agilité
* [Holacracy](http://labdsurlholacracy.com/bande-dessinee-holacracy)

## DNS
* [Arrêtez (de conseiller) d'utiliser Google Public DNS](https://www.shaftinc.fr/arretez-google-dns.html)
* [Utiliser un résolveur DNS public ?](http://www.bortzmeyer.org/dns-resolveurs-publics.html)

## Divers
* [Comment s'est formée la Lune ?](http://www.msn.com/fr-fr/actualite/technologie-et-sciences/comment-sest-form%C3%A9e-la-lune-voici-les-cinq-hypoth%C3%A8ses-des-scientifiques/ar-AAlSqGa?OCID=ansmsnnews11)
* [Parution de la quatrième édition du Guide d’autodéfense numérique](https://guide.boum.org/news/quatrieme_edition/)
* [La biere et les couches - data mining](http://binaire.blog.lemonde.fr/2017/01/16/mais-pourquoi-la-biere-et-les-couches/)
* [Le bug avec les idéologies du Net](http://tcrouzet.com/2017/01/15/le-bug-avec-les-ideologies-du-net/)
* [The mind behind Linux](http://www.ted.com/talks/linus_torvalds_the_mind_behind_linux#t-139911)