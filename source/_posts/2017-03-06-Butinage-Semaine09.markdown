---
layout: post
title: "Mes butinages de la semaine 09"
date: 2017-02-26 00:42:00 +0000
comments: true
categories: [agilité, liberté, softwarecraftmanship]
---

Voilà, je fais part de mes butinages hebdomadaires. Dans mon cheminement d'agiliste, j'ai décidé de ne plus envoyé mes lectures à mes connaissances.
Je passe d'un flux poussé à un flux tiré :-)
Ce qui veut dire que je vais publier cette collecte et viendront voir ceux qui sont intéréssés.

**Bonne butinage !**

# Butinages de la semaine 09!

## Artisanant logiciel
* [Necessary Comments](http://blog.cleancoder.com/uncle-bob/2017/02/23/NecessaryComments.html)
* [Des développeurs avouent leurs « péchés » de programmation. Pour s'ériger contre un processus d'entretien technique qu'ils estiment brisé](http://www.developpez.com/actu/120690/Des-developpeurs-avouent-leurs-peches-de-programmation-pour-s-eriger-contre-un-processus-d-entretien-technique-qu-ils-estiment-brise/)

## Vie privée - indépendance numérique
* [Être un géant du mail, c’est faire la loi…](https://framablog.org/2017/02/17/etre-un-geant-du-mail-cest-faire-la-loi/)
* [SHA-1 : des chercheurs prouvent l'exploitation des collisions dans une attaque](https://www.nextinpact.com/news/103436-sha-1-chercheurs-prouvent-exploitation-collisions-dans-attaque.htm)
* [GNU Privacy Guard - GnuPG](https://doc.ubuntu-fr.org/gnupg)
* [Les conséquences techniques de l'interception HTTPS en entreprise](http://www.bortzmeyer.org/https-interception.html)

## Divers
* [Identifiez ce qui vous empêche de prendre des initiatives.](http://techtrends.eu/identifiez-ce-qui-vous-empeche-de-prendre-des-initiatives/)
* [Télétravail et burn-out](http://theconversation.com/teletravail-et-burn-out-73236)
* [Coup de gueule de Linus Torvalds](http://lkml.iu.edu/hypermail/linux/kernel/1702.2/05171.html)
* [Certified Malice](https://textslashplain.com/2017/01/16/certified-malice/)
* [Mozilla y Debian ponen fin a más de una década de conflicto](http://www.muylinux.com/2017/03/01/mozilla-debian-firefox)