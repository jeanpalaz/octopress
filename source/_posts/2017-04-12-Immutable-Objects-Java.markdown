---
layout: post
title: "Objets Immuables en Java mémo"
date: 2017-04-12 00:42:00 +0000
comments: true
categories: [code, Java, immutable]
---

Après la lecture de cette article [Immutable Objects in Java](https://dzone.com/articles/immutable-objects-in-java), je me suis fait un petit mémo sous forme de mindmap.
Je partage cette carte. 

{% img https://jeanpalaz.gitlab.io/images/ImmutableObjectsJava.png [Immutable Objects Java Mindmap] %}

Happy Coding !