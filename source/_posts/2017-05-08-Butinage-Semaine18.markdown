---
layout: post
title: "Mes butinages de la semaine 18"
date: 2017-05-08 00:42:00 +0000
comments: false
categories: [liberté, softwarecraftmanship, hautdebit, algorithme , numerique]
---

** Bon butinage !**

## Artisanat logiciel
* [Programmation : découvrez les sept raisons pour lesquelles vous devez apprendre le langage Python](https://www.developpez.com/actu/133538/Programmation-decouvrez-les-sept-raisons-pour-lesquelles-vous-devez-apprendre-le-langage-Python-selon-myTectra/)

## Internet
* [Hâte-toi lentement : le très haut débit qui fait débat.](http://affordance.typepad.com//mon_weblog/2017/04/le-haut-debit-fait-debat.html)
* [Savez-vous que les ports utilisés par vos applications peuvent être exploités par des pirates ?](https://homputersecurity.com/2017/05/01/savez-vous-que-les-ports-utilises-par-vos-applications-peuvent-etre-exploites-par-des-pirates/)
* [Présidentielle 2017 : Macron et Le Pen, deux visions opposées des enjeux numériques](http://www.latribune.fr/technos-medias/presidentielle-2017-macron-et-le-pen-deux-visions-opposees-des-enjeux-numeriques-696526.html)
* [J'ai testé les algorithmes de Facebook et ça a rapidement dégénéré](http://ici.radio-canada.ca/nouvelle/1029916/experience-facebook-algorithmes-bulle-desinformation)