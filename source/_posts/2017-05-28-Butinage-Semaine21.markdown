---
layout: post
title: "Mes butinages de la semaine 21"
date: 2017-05-28 00:42:00 +0000
comments: false
categories: [liberté, vie-privée, softwarecraftmanship, wannacry]
---

** Bon butinage !**

## Artisanat logiciel
* [Error handling - Returning Results](https://dev.to/michael_altmann/error-handling---returning-results)

## Vie privée
* [Facebook, le Brexit et les voleurs de données](https://framablog.org/2017/05/22/facebook-brexit-voleurs-donnees/)

## Divers
* [WannaCry et le chiffrement](https://blog.cozycloud.cc/post/2017/05/17/WannaCry-et-le-chiffrement?lang=fr)