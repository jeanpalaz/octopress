---
layout: post
title: "Mes butinages de la semaine 23"
date: 2017-06-11 00:42:00 +0000
comments: false
categories: [null, pairprogramming, softwarecraftmanship, pomodoro, vieprivée, piratage]
---

** Bon butinage !**

## Artisanat logiciel
* [Why NULL references are a bad idea](https://dev.to/0x13a/why-null-references-are-a-bad-idea)
* [My Experience with Pair Programming](https://dev.to/raulavila/my-experience-with-pair-programming)
* [Zen and the art of programming](https://dev.to/rodneyringler/zen-and-the-art-of-programming)

## Vie privée
* [Sexe, Internet & Politique : tout le monde ment, mais Google sait](https://usbeketrica.com/article/sexe-internet-politique-tout-le-monde-ment-mais-google-sait)

## Agile
* [Pomodoro](http://www.pomodorotechnique.com/)

## Divers
* [Le piratage de TV5 Monde vu de l’intérieur](http://www.lemonde.fr/pixels/article/2017/06/10/le-piratage-de-tv5-monde-vu-de-l-interieur_5142046_4408996.html)
