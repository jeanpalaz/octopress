---
layout: post
title: "Mes butinages de la semaine 02"
date: 2017-01-15 00:42:00 +0000
comments: true
categories: [Blog, Test, Junit, legacy, code, softwarecraftmanship, technical, debt]
---

Voilà, je fais part de mes butinages hebdomadaires. Dans mon cheminement d'agiliste, j'ai décidé de ne plus envoyé mes lectures à mes connaissances.
Je passe d'un flux poussé à un flux tiré :-)
Ce qui veut dire que je vais publier cette collecte et viendront voir ceux qui sont intéréssés.

**Bon butinage !**

# Butinages de la semaine 02!

## Artisanat logiciel
* [Singleton or not](http://blog.cleancoder.com/uncle-bob/2015/07/01/TheLittleSingleton.html)
* [Kata TripService](https://github.com/sandromancuso/trip-service-kata)
* [10 things your static language can’t do](https://melix.github.io/blog/2014/12/10-things-static-cant-do.html)
* [Dépendances statiques](http://blog.xebia.fr/2015/01/23/legacy-code-se-defaire-des-dependances-statiques/)
* [Kata TripService - @sandromancuso](https://www.youtube.com/watch?v=_NnElPO5BU0)

## Agilité
* [Holacratie](http://labdsurlholacracy.com/bande-dessinee-holacracy)

## Raspberry PI
* [MagPi FR](https://www.raspberrypi.org/magpi-issues/MagPi_Mini_French_02.pdf)
* [Death Star](http://www.geeky-gadgets.com/awesome-raspberry-pi-death-star-vision-display-09-01-2017/)

## Auto hebergement
* [Mail - SPF](https://www.octopuce.fr/spf-est-ce-vraiment-utile-pour-le-mail/)

## Divers
* [Hackathon, piège à cons - Tris Acatrinei](https://www.hackersrepublic.org/culture-du-hacking/hackathon-piege-cons)
* [Neutralité du Net](https://www.nextinpact.com/news/102758-neutralite-net-operateurs-americains-attaquent-deja-obligations-sur-vie-privee.htm)
* [Une PME florissante pillée par un hacker](http://www.alamosoft.com/blog/2017/01/08/pme-florissante-pillee-hacker/)
* [Adiós a GNU Libreboot](http://www.muylinux.com/2017/01/09/adios-a-gnu-libreboot)
* [L'histoire du créateur de la science des erreurs et de ses erreurs](http://www.slate.fr/story/133685/createur-science-erreurs)
