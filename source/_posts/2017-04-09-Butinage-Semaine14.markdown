---
layout: post
title: "Mes butinages de la semaine 14"
date: 2017-04-09 00:42:00 +0000
comments: true
categories: [vie-privée, code, shellCheck, bash, algorithme, bureaucratie]
---

** Bon butinage !**

## Artisanat logiciel
* [Vérifier un script Bash avec ShellCheck](http://blog.shevarezo.fr/post/2017/04/05/shellcheck-verifier-script-bash)

## Sécurité, vie privée
* [Personne n’est anonyme sur Internet, pas même le patron du FBI](http://www.lemonde.fr/big-browser/article/2017/03/31/personne-n-est-anonyme-sur-internet-pas-meme-le-patron-du-fbi_5103964_4832693.html)
* [Windows 10 : Microsoft en dit plus sur les données collectées](http://www.zdnet.fr/actualites/windows-10-microsoft-en-dit-plus-sur-les-donnees-collectees-39850862.htm)

## Divers
* [Les algorithmes sont-ils une nouvelle forme de bureaucratie ?](http://www.internetactu.net/2017/04/04/les-algorithmes-sont-ils-une-nouvelle-forme-de-bureaucratie/)
* [CPU, Ex0051 Partir pour la Silicon Valley](https://cpu.dascritch.net/post/2017/03/23/Ex0051-Partir-pour-la-Silicon-Valley)