---
layout: post
title: "Mes butinages de la semaine 19"
date: 2017-05-14 00:42:00 +0000
comments: false
categories: [softwarecraftmanship, testdouble, coverage, java, vieprivée, thunderbird, psychopathe]
---

** Bon butinage !**

## Artisanat logiciel
* [On Getting Old(er) in Tech](http://corgibytes.com/blog/2016/12/06/getting-old-er-in-tech/)
* [The tragedy of 100% code coverage](https://dev.to/danlebrero/the-tragedy-of-100-code-coverage)
* [Test Doubles - Fakes, Mocks and Stubs](https://dev.to/milipski/test-doubles---fakes-mocks-and-stubs) 
* [Vous pensiez être débarrassé de Java ? Hahahahahaha…](https://blog.seboss666.info/2017/05/vous-pensiez-etre-debarrasse-de-java-hahahahahaha/)

## Internet
* [Les techniques du roi des hackers pour surfer le web anonymement ](http://www.01net.com/actualites/les-techniques-du-roi-des-hackers-pour-surfer-le-web-anonymement-1157929.html)
* [Des nouvelles de notre ami Facebook – mai 2017](https://framablog.org/2017/05/10/nouvelles-ami-facebook/)
* [Moi ministre du numérique, par Benjamin Bayart: reprendre tout dans le bon sens](http://www.slate.fr/tribune/85725/benjamin-bayart-moi-ministre-du-numerique-tribune)

## Vie privée
* [Runalyze : faites du sport, gardez votre vie privée !](https://www.jgachelin.fr/runalyze-faites-du-sport-gardez-votre-vie-privee/)

## Divers
* [Mozilla accepte d'être le tuteur fiscal et légal de Thunderbird](https://www.developpez.com/actu/135983/Mozilla-accepte-d-etre-le-tuteur-fiscal-et-legal-de-Thunderbird-mais-le-client-de-messagerie-sera-gere-de-maniere-autonome-au-niveau-operationnel/)
* [Si vous êtes psychopathe, cet article vous fera rire](http://www.slate.fr/story/143645/psychopathes-article-vous-fera-rire)
