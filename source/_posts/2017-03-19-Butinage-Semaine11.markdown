---
layout: post
title: "Mes butinages de la semaine 11"
date: 2017-03-19 00:42:00 +0000
comments: true
categories: [agilité, liberté, softwarecraftmanship]
---


**Bonne butinage !**

# Butinages de la semaine 11!

## Artisanat logiciel
* [TDD Harms Architecture](http://blog.cleancoder.com/uncle-bob/2017/03/03/TDD-Harms-Architecture.html)
* [The Three Laws of Test-Driven Development](http://programmer.97things.oreilly.com/wiki/index.php/The_Three_Laws_of_Test-Driven_Development)
* [Legacy club](https://www.infoq.com/fr/presentations/lkfr-boucard-pierrain-legacy-club)

## Logiciel libre
* [Wikileaks dévoile comment Microsoft a tué le Logiciel Libre dans la Tunisie de Ben Ali](http://www.fhimt.com/2011/09/06/wikileaks-devoile-comment-microsoft-a-tue-le-logiciel-libre-dans-la-tunisie-de-ben-ali/)

## Sécurité, vie privée
* [Termes expliqués n°6 : la sécurité (les attaques, et les défenses)](https://blog.seboss666.info/2017/03/termes-expliques-n6-la-securite-les-attaques-et-les-defenses/)
* [Des webcams Wi-Fi chinoises victimes d'importantes failles de sécurité](https://www.nextinpact.com/news/103657-des-webcams-wi-fi-chinoises-victimes-dimportantes-failles-securite.htm)
* [Les internautes négligent-ils sécurité et vie privée sur Internet ? Souvent](http://www.zdnet.fr/actualites/les-internautes-negligent-ils-securite-et-vie-privee-sur-internet-souvent-39849766.htm)

## Divers
* [Un demi-cerveau suffit](https://blog.hackteskids.com/un-demi-cerveau-suffit-4b37a4326333)
* [Agile Practices Timeline](https://www.agilealliance.org/agile101/practices-timeline/)
* [Sans fessée comment faire?](http://data.over-blog-kiwi.com/1/05/26/48/20170317/ob_01bdf0_sans-fesse-e-17-mars-2.pdf)